#!/snap/bin/pwsh -NoLogo
# Called by setup.sh
# Initial powershell / powercli setup
Install-Module VMware.PowerCLI
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false

