#!/bin/bash

# Dumb bash -> pwsh wrapper mostly because https://unix.stackexchange.com/questions/399690/multiple-arguments-in-shebang

# Expected installation:
#  /somehwere/terrashell == git repo
#  ln -s /somehwere/terrashell/terrashell /somehting/in/path/terrashell
# (This converts /somehting/in/path/terrashell -> /somehwere/terrashell/plugins)
SCRIPT_PATH="$(readlink -f """${BASH_SOURCE[0]}""")"
export TERRASHELL_PLUGINS="$(dirname """${SCRIPT_PATH}""")/plugins"
/snap/bin/pwsh -NoExit -NoLogo "${SCRIPT_PATH}.ps1" $@

# Basic examples:

# By default, Get-Terrastate will be equivalent to Get-Terrastate -Type VM -Output ID
# (which really translates to terraform state pull | ./powercli/state-query.py --type vsphere_virtual_machine -o moid )

# The output of Get-Terrastate is IDs that can be directly piped to Manage-Snapshots or Manage-VMs.

# Reports:
#Get-TerraState | Manage-Snapshots -Action GetReport
#Get-TerraState | Manage-VMs -Action GetReport

# Basic Snapshot Management:
#Get-TerraState | Manage-Snapshots -Action New
#Get-TerraState | Manage-Snapshots -Action RevertToLast
#Get-TerraState | Manage-Snapshots -Action Backup

# Basic VM Management:
#Get-TerraState | Manage-VMs -Action Stop
#Get-TerraState | Manage-VM -Action Start

# In-Place resource upgrade (e.g. Team Firewalls):
# Check your Regex with: Get-TerraState -Regex 'vm_fw' -Output Name
# $fw = Get-TerraState -Regex 'vm_fw'
# $fw | Manage-VMs -Action Stop -Confirm:$false
# $fw | Manage-VMs -Action Set -NumCpu 2 -MemoryGB 4 -Confirm:$false
# $fw | Manage-VMs -Action Start 

# Note: You can do custom things by piping around IDs or names.
# Get-VM -ID (Get-Terrastate) | MyCustomThing
# e.g. you could set folder permissions like this:
# Get-Folder -Id (Get-Terrastate -Type Folder)  | MyCustomThing
# Get-Folder -Id (Get-Terrastate -Type Folder)  | MyCustomThing

#Or, if you need even more granularity, you can use the more advanced features of Get-Terrastate
# Output the "path" and "id" fields of the Terraform state (tab delimited) if the resource name matches a Regex.
# Get-TerraState -Type Folder -RawOutput id,path -Regex '.*Team..$'
# (This will return the folder IDs + the folder path for Teams only - infrastructure folders excluded)
#Folder-group-v16234    Purple Team/Terraform/Testing/ITSec/Team01
#Folder-group-v16236    Purple Team/Terraform/Testing/ITSec/Team02
#Folder-group-v16235    Purple Team/Terraform/Testing/ITSec/Team03

# NOTE: if you are using multiple ouputs with RawOutput, the first must be an ID (either moid for VMs or id for Folders).

