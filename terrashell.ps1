#!/snap/bin/pwsh -NoExit
param(
  [parameter(Mandatory=$false)] [switch] $q
)

if ($q){
    function global:Write-Host() {}
}

$MgmtColor = "green"
Write-Host "Welcome to TerraShell: An Automation-Friendly Infrastructure Management Shell!" -ForeGroundColor $MgmtColor

Write-Verbose "Loading Custom Functions..."

$PluginsRoot=$Env:TERRASHELL_PLUGINS
$NumPlugins = 0
Get-ChildItem $PluginsRoot | %{
    if ($_.FullName -Like "*.ps1"){
        . $_.FullName
        if ($?){
            Write-Host "Loaded $($_.Name)" -ForeGroundColor $MgmtColor
            $NumPlugins += 1
        } else {
            Write-Warning "Failed to load $($_.Name)"
        }
    }
}

Write-Verbose "Connecting to vCenter..."
Terra-Connect
if ($?) {
  Write-Host "Connected to vCenter." -ForeGroundColor $MgmtColor
}

if ($q){
    Remove-Item -Path Function:/Write-Host
}

