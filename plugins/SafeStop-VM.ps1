#!/snap/bin/pwsh -NoLogo

# Stop the VM as nicely as possible
function SafeStop-VM {

[CmdletBinding(SupportsShouldProcess, ConfirmImpact='High')]
param(
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(Mandatory=$false)] [int] $Timeout = 30,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $VM
)
    $Caption = "SafeStop-VM"
    if ($Name){
        $VM = Get-VM $Name
    } else {
        $VM = @($input)
    }

    if ( -not $VM) {
        Write-Error "No VMs given" -Category InvalidArgument
        return
    }

    $failed = New-Object System.Collections.Generic.List[System.Object]
    foreach ($v in $VM) {
        $force = $false

        if ( $v.PowerState -eq "PoweredOff") {
            # It's already off
            continue
        }
        elseif ( $v.PowerState -eq "Suspended") {
            if ($PSCmdlet.ShouldProcess($Caption, "$v : Power off suspended VM?", $Caption) ) { 
                $v | Stop-VM -Confirm:$false
                $pending += $v
            }
        }
        elseif ( $v.ExtensionData.Guest.ToolsStatus -eq "toolsOk" ) {
            if ($PSCmdlet.ShouldProcess($Caption, "$v : Shutdown Guest OS?", $Caption) ) { 
                $v | Stop-VMGuest -Confirm:$false
                if (!$?){
                    $force = $true
                }
                $pending += $v
            }
        } else {
            $force = $true
        }

        if ($force -and $PSCmdlet.ShouldProcess($Caption, "$v : Shut off VM forefully?", $Caption) ) { 
            $v | Stop-VM -Confirm:$false -RunAsync
            $pending += $v
        }

    }
    
    # block until all VMs are powered off
    while ($pending) {
        Write-Host "Waiting for VMs to shut down..."
        Start-Sleep $Timeout
        $failed.Clear()

        # Refresh power state
        $IDs = $pending | %{ $_.Id }
        $pending = Get-VM -Id $IDs
        foreach($v in $pending) {
            if ($v.PowerState -ne "PoweredOff") {
                if ($PSCmdlet.ShouldProcess($Caption, "$v : Shut off VM forefully?", $Caption) ) {
                    $v | Stop-VM -Confirm:$false
                }

               $failed.Add($v)
            }
        }
        $pending = $failed.ToArray()
    }

}
