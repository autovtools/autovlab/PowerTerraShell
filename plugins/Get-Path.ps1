#!/snap/bin/pwsh -NoLogo

# For some reason, `Get-Folder` cannot accept a full folder path.
# The PowerCLI way to access a path like "Purple Team/Terraform/Testing/ITSec/Team01/LAN" is:
# Get-Folder "Purple Team" | Get-Folder "Terraform" | Get-Folder "Testing" | Get-Folder "ITSec" | Get-Folder "Team01" | Get-Folder "LAN"

# Or, you can use:
# Get-Path "Purple Team/Terraform/Testing/ITSec/Team01/LAN"
# (Note: use Ids if you can; names may change)

# All PowerCLI options for Get-Folder are passed through directly to Get-Folder
# WARNING: Assumes '/' is the folder delimiter


function Get-Path {
  param (
    [Parameter(Mandatory=$false)] [String[]] $Name,
    [Parameter(Mandatory=$false)] [String[]] $Id,
    [Parameter(ValueFromPipeline=$true)] [Object[]] $Location,
    [Parameter(Mandatory=$false)] [Switch]   $NoRecursion=$false,
    [Parameter(Mandatory=$false)] [Object[]] $Server,
    [Parameter(Mandatory=$false)] [Object[]] $Type
  )

  $folder_args = @{
    Id = $Id
    Location = $Location
    NoRecursion = $NoRecursion
    Server = $Server
    Type = $Type
  }
  
  $set_args = @{}
  $folder_args.Keys | % { if ($folder_args[$_]) { $set_args[$_] = $folder_args[$_] } }

  ForEach ($n in $Name) {
    Write-Verbose "Spliting $n by '/'..."
    $curr = $null
    ForEach ($folder in $n.Split("/")) {
      if ($folder){
          Write-Verbose "Curr: $curr . Getting: $folder"
          $set_args["Name"] = $folder
          $curr = $curr | & Get-Folder @set_args
      }
    }
    $curr
  }
  if (! $Name) {
    # Full passthru
    & Get-Folder @set_args
  }

  Write-Verbose "Get-Folder $folder_args"
}
