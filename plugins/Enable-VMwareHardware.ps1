#!/snap/bin/pwsh -NoLogo

# Upgrades the NetworkAdapter and SCSiController to the better versions that require VMWare-tools
#   (Useful when you have to bootstrap very old guests with generic hardware in order to install VMware tools)
# WARNING: If you did not install the vmware drivers in the guest correctly, this will stop it from booting
function Enable-VMwareHardware{

[CmdletBinding()]
param(
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $VM,
  [string] $NetworkAdapterType = "Vmxnet3",
  [string] $SCSIControllerType = "Paravirtual",
  [string] $SnapshotDesc
)
    if ($Name){
        $VM = Get-VM $Name
    } else {
        $VM = @($input)
    }

    if ( -not $VM) {
        Write-Error "No VMs given" -Category InvalidArgument
        return
    }

    Get-NetworkAdapter -VM $vm | Set-NetworkAdapter -Type $NetworkAdapterType -Confirm:$false | Out-Null
    Get-ScsiController -VM $vm | Set-ScsiController -Type $SCSIControllerType -Confirm:$false | Out-Null

}
