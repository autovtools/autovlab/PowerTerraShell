#!/snap/bin/pwsh -NoLogo

# Edit vApp settings to change settings like startDelay, stopAction, and stopDelay
# The vSphere default is slow (wait 120 seconds between each VM; start each vApp separately)
# This script will put all VMs in the same start group and apply the requested settings to everything.
# Setting this via Terraform doesn't currently work, and the gui menu
#   (Accessible through right click > Edit settings > StartOrder)
# Is painful to set manually

# https://vwiki.co.uk/vApp_Script_Extracts_and_Examples
# https://communities.vmware.com/thread/581979
# https://pubs.vmware.com/vsphere-51/index.jsp?topic=%2Fcom.vmware.wssdk.apiref.doc%2Fvim.vApp.VAppConfigSpec.html

function Set-vAppConfig{

[CmdletBinding()]
param(
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $vApp,
  [int] $StartDelay = 0,
  [int] $StopDelay = 120,
  [switch] $WaitingForGuest,
  [ValidateSet("guestShutdown", "powerOff", "suspend", "none")]
    [string] $StopAction = "guestShutdown",
  [ValidateSet("powerOn", "off")]
    [string] $StartAction = "powerOn"
)
    if ($Name){
        $vApp = Get-vApp $Name
    } else {
        $vApp = @($input)
    }

    if ( -not $vApp) {
        Write-Error "No vApps given" -Category InvalidArgument
        return
    }

    [System.Collections.ArrayList]$vAppList = @();
    
    $children = $vApp
    while($children) {
        foreach($child in $children){
            $vAppList.Add($child) | Out-Null
        }
        $children = ($children | Get-vApp)
    }

    foreach($app in $vAppList) {
        # Get view of vApp
        $vAppView = Get-View $app

        # Prep reconfig object
        $vAppConfigSpec = New-Object VMware.Vim.VAppConfigSpec
        $vAppConfigSpec.entityConfig = New-Object VMware.Vim.VAppEntityConfigInfo[] ($vAppView.vAppConfig.EntityConfig.Count)

        # Go through each child and check config is as we wish
        $i = 0
        foreach ($vm in $vAppView.vAppConfig.EntityConfig) {
            $vAppConfigSpec.entityConfig[$i] = New-Object VMware.Vim.VAppEntityConfigInfo
            $vAppConfigSpec.entityConfig[$i].key = $vm.Key
            $vAppConfigSpec.entityConfig[$i].tag = $vm.Tag
            $vAppConfigSpec.entityConfig[$i].startOrder = 1
            $vAppConfigSpec.entityConfig[$i].startDelay = $StartDelay
            $vAppConfigSpec.entityConfig[$i].startAction = $StartAction
            $vAppConfigSpec.entityConfig[$i].waitingForGuest = $WaitingForGuest
            $vAppConfigSpec.entityConfig[$i].stopDelay = $StopDelay
            $vAppConfigSpec.entityConfig[$i].stopAction = $StopAction
            $i ++
        }

        $vAppView.UpdateVAppConfig($vAppConfigSpec)
    }
}
