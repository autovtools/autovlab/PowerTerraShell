#!/snap/bin/pwsh -NoLogo

# Enable copy / paste
# VMs must be off
# Requires VMware tools (to work)
function Enable-CopyPaste{

[CmdletBinding()]
param(
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $VM
)
    if ($Name){
        $VM = Get-VM $Name
    } else {
        $VM = @($input)
    }

    if ( -not $VM) {
        Write-Error "No VMs given" -Category InvalidArgument
        return
    }

    $VM | New-AdvancedSetting -Name isolation.tools.copy.disable -Value FALSE -Confirm:$false -Force:$true
    $VM | New-AdvancedSetting -Name isolation.tools.paste.disable -Value FALSE -Confirm:$false -Force:$true

}
