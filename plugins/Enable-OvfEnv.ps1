#!/snap/bin/pwsh -NoLogo

# Enable vApp Options and selet OVF environment transport = VMware Tools
#   This exposes the Ovf Environment to the Guest OS,
#   Allowing it to read some useful info

# https://code.vmware.com/forums/2530/vsphere-powercli#575077
# https://www.virtuallyghetto.com/2012/06/ovf-runtime-environment.html

# Ex) pfSense GuestInfo customization uses this to read the MAC addresses
#   in the order they appear in vSphere.
#   This lets us sort our interface list in the guest to match.
function Enable-OvfEnv{

[CmdletBinding()]
param(
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $VM,
  [switch] $Snapshot = $true,
  [string] $SnapshotName,
  [string] $SnapshotDesc
)
    if ($Name){
        $VM = Get-VM $Name
    } else {
        $VM = @($input)
    }

    if ( -not $VM) {
        Write-Error "No VMs given" -Category InvalidArgument
        return
    }

    $spec = New-Object VMware.Vim.VirtualMachineConfigSpec
    $spec.VAppConfig = New-Object VMware.Vim.VmConfigSpec
    $spec.VAppConfig.OvfEnvironmentTransport = @('com.vmware.guestInfo')

    # This is somehow valid syntax, even for a list
    $VM.ExtensionData.ReconfigVM($spec) 

}
