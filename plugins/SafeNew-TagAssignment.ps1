#!/snap/bin/pwsh -NoLogo

# Like New-TagAssignment,
#   but creating new tag categories and new tags as needed
function SafeNew-TagAssignment{

[CmdletBinding()]
param(
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $VM,
  [parameter(Mandatory=$true)] [string[] ] $TagNames,
  [parameter(Mandatory=$true)] [string[] ] $TagCategories,
  [parameter(Mandatory=$false)] [switch] $Force
)
    if ($TagNames.length -ne $TagCategories.length) {
        Write-Error "TagNames and TagCategories must be of equal length" -Category InvalidArgument
        return
    }
    if ($Name){
        $VM = Get-VM $Name
    } else {
        $VM = @($input)
    }

    if ( -not $VM) {
        Write-Error "No VMs given" -Category InvalidArgument
        return
    }

    foreach ($v in $VM){
        for ($i = 0; $i -lt $TagNames.length; $i++){
            Get-TagCategory $TagCategories[$i] 2>&1 | Out-Null
            if (!$?){
                Write-Verbose ("Creating new TagCategory: {0}" -f $TagCategories[$i])
                New-TagCategory -Name $TagCategories[$i] -Cardinality Single 2>&1 | Out-Null
            }
            Get-Tag -Name $TagNames[$i] -Category $TagCategories[$i] 2>&1 | Out-Null
            if (!$?){
                Write-Verbose ("Creating new Tag: {0}" -f $TagNames[$i])
                New-Tag -Name $TagNames[$i] -Category $TagCategories[$i] 2>&1 | Out-Null
            }
            if ($Force){
                $v | Get-TagAssignment -Category $TagCategories[$i] | Remove-TagAssignment -Confirm:$false 2>&1 | Out-Null
            }
            $tag = Get-Tag -Name $TagNames[$i] -Category $TagCategories[$i]
            if (!$tag){
                # The vSphere session is probably broken; seems to happen randomly
                Write-Error ("[$v] Failed to create tag[$i]: {0} -> {1}" -f $TagCategories[$i], $TagNames[$i] )
                return
            } else {
                $v | New-TagAssignment -Tag $tag
            }

        }
    }

}
