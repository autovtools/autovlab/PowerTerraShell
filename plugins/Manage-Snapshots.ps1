#!/snap/bin/pwsh -NoLogo

# PowerShell pipelines are weird :(
# Best - http://www.powertheshell.com/input_psv3/
# https://learn-powershell.net/2013/05/07/tips-on-implementing-pipeline-support/

# Because of how user interaction / consoles work, either run in a pwsh shell
# (where you will be prompted for confirmation on deletes by default)
# or use -Confirm:$false from bash if you are running non-interactively

function Manage-Snapshots {

[CmdletBinding(SupportsShouldProcess, ConfirmImpact='High')]
param(
  [parameter(Mandatory=$false)] [String[] ] $IDs,
  [parameter(Mandatory=$true)] [String] $Action,
  [parameter(Mandatory=$false)] [Switch] $Connect=$true,
  [parameter(Mandatory=$false)] [Switch] $Memory=$false,
  [parameter(Mandatory=$false)] [Switch] $Quiesce=$false,
  [parameter(Mandatory=$false)] [String] $Name,
  [parameter(Mandatory=$false)] [Int] $Retain=4,
  [AllowEmptyString()][String] $DesiredPowerState="",
  [Int] $SuspendDelay=120,
  [parameter(ValueFromPipeline=$true)]
    [String[]] $InputObject
)

# https://stackoverflow.com/questions/20874464/format-table-on-array-of-hash-tables
function MakeTable {
  param (
    [Object[] ] $Rows,
    [String []] $Columns
  )
  return $Rows | %{ [PSCustomObject]$_ } | Format-Table -Property $Columns -AutoSize
  
}
# RemoveFirst and RemoveLast are really the same, one just deletes [0] and the other deletes [-1]
function RemoveOne {
[CmdletBinding(SupportsShouldProcess, ConfirmImpact='High')]
 param(
  [Object[] ]$VM,
  [Int] $index=0,
  [String] $Caption,
  [String] $Desc
 )
  # Get-Snapshot + Remove-Snapshot
  ForEach ($v in $VM) {
    #Write-Host $v 
    $snapshots = $v | Get-Snapshot
    $num_snapshots = $snapshots.Length
    if ($num_snapshots -gt 0) {
      if ($num_snapshots -eq 1) {

        if ($PSCmdlet.ShouldProcess($Caption, "Warning: VM $v only has 1 snapshot. Delete it anyway?", $Caption) ) {
          Write-Host "$v : Deleting only snapshot"
          $snapshots[$index] | Remove-Snapshot -Confirm:$false
          
        } else {
          Write-Host "Delete canceled"
        }
      } else {
        if ($PSCmdlet.ShouldProcess($Caption, "VM $v has $num_snapshots snapshots. Delete the $Desc ?", $Caption) ) {
          Write-Host "$v : Deleting $Desc of $num_snapshots snapshots"
          $snapshots[$index] | Remove-Snapshot -Confirm:$false
        } 
      } 
    } else {
        Write-Warning "$v : No snapshots found!"
    }

  }
}

function Prune {
[CmdletBinding(SupportsShouldProcess, ConfirmImpact='High')]
  param(
    [Object[]] $VM,
    [String] $Caption,
    [Switch] $NoInteractive=$false,
    [Int] $Retain=-1
  )

    ForEach ($v in $VM) {
      $snapshots = Get-Snapshot -VM $v
      $num_snapshots = $snapshots.Length
      $size_gb = 0.0
      $snapshots | %{ $size_gb += $_.SizeGB }
      $size_gb = [math]::Round($size_gb, 2)
      $msg = "VM $v has $num_snapshots snapshots ($size_gb GB total). "
      $ActualRetain = $Retain
      if ($Retain -lt 0) {
        $ActualRetain = [math]::max($num_snapshots + $Retain, 0)
      }
      if ( $num_snapshots -gt $ActualRetain ) {
        if ($Retain -lt 0) {
          if (-1 * $Remain -ge $num_snapshots) {
            Write-Warning "VM $v only has $num_snapshots, but we are Pruning $(-1 * $Remain). All snapshots for this VM will be deleted."
            $msg += "Remove **ALL** of them?"
          }
          else {
            $msg += "Remove $(-1 * $Retain ) of them?"
          }
        } else {
          $msg += "Remove all but $ActualRetain ?"
        }
        if ($Retain -eq 0) {
          Write-Warning "VM $v : Retain=0 is equivalent to RemoveAll. All snapshots for this VM will be deleted."
        }
        if ($NoInteractive -or $PSCmdlet.ShouldProcess($Caption, $msg, $Caption) ) {
          ForEach ($index in $( & $SelectSnapshotsScript $num_snapshots $ActualRetain )  ) {
            Write-Host "$v : Deleting Snapshot # $([int]$index + 1) of $num_snapshots..."
            $snapshots[$index] | Remove-Snapshot -Confirm:$false
          }
          
        }
      } else {
        Write-Verbose "$v : $num_snapshots exist out of the $Retain allowed - nothing to do."
      }

    }

}

function Revert {
[CmdletBinding(SupportsShouldProcess, ConfirmImpact='High')]
  param(
    [Object[] ]$VM,
    [Int] $index=-1,
    [String] $Caption,
    [String] $Desc,
    [AllowEmptyString()][String] $DesiredPowerState="",
    [Int] $SuspendDelay=120
  )
    $OriginalDesiredPowerState = $DesiredPowerState
    $to_suspend = New-Object System.Collections.Generic.List[System.Object]
    ForEach ($v in $VM) {
      $snapshots = Get-Snapshot -VM $v
      $num_snapshots = $snapshots.Length
    
      if ($snapshots.Length -gt 0) {
        $snap= $snapshots[$index]
        if (! $OriginalDesiredPowerState) {
         $DesiredPowerState = $v.PowerState
        }

        if ($PSCmdlet.ShouldProcess($Caption, 
@"
Revert $v to $snap ?
Current State: $($v.PowerState)
Snapshot State: $($snap.PowerState)
Desired State: $DesiredPowerState

WARNING: Any changes since the snapshot will be lost; this cannot be undone.",
"@
, $Caption) ) {
          Set-VM -VM $v -Snapshot $snap -Confirm:$false
          if ($snap.PowerState -eq "PoweredOff") {
            if ($DesiredPowerState -ne "PoweredOff") {
              Write-Host "Starting $v ..."
              Start-VM -VM $v
              # If user wants to "suspend" after revert, its probably because they want it to boot fast
              # So, we don't want to suspend until after the boot is finished
              # (we'll come back and do it later)
              if ($DesiredPowerState -eq "Suspended") {
                Write-Host "$v will be suspended later..."
                $to_suspend.Add($v)
              }
            } 
          } elseif ($snap.PowerState -eq "PoweredOn") {
            if ($DesiredPowerState -eq "PoweredOff") {
              Write-Host "Stopping $v ..."
              # We reverted to snapshot after confirmation, so turning the VM off doesn't need its own confirmation 
              Write-Host "Waiting a bit for VMWare Tools to start..."
              Start-Sleep -Seconds 2
              Stop-VMGuest -VM $v -Confirm:$false 
              if (! $?) {
                $timeout = 15
                Write-Host "$v : Shutdown Guest OS failed - Trying again in $timeout seconds ..."
                Start-Sleep -Seconds $timeout
              
                if (! $?) {
                  Write-Host "$v : Shutdown Guest OS failed again - Powering Off ..."
                  Stop-VM -VM $v -Confirm:$false
                }
              }
            } elseif($DesiredPowerState -eq "Suspended") {
              # We reverted to a PoweredOn VM, so it is safe to suspend now
              Suspend-VM -VM $v
            }
          }
          # Assert a snapshot can't have a state of Suspended
    
        }
      } else {
        Write-Warning "$v : No snapshots found!"
      }
    } # End of ForEach VM

    # Warning: this is a weird use case, so it might be buggy
    if ($to_suspend.Length -gt 0 ) {
      # Because we changed the PowerStates, we need to check the current PowerStates
      $IDs = $to_suspend | %{ $_.Id }
      $not_suspended = Get-VM -Id $IDs | ? { $_.PowerState -ne "Suspended" }
      Write-Host "$($not_suspended.Length) VMs to suspend:"
      $not_suspended | Format-Table -AutoSize
      if ($not_suspended.Length -gt 0) {
        Write-Host "$($not_suspended.Length) VMs need to be suspended.`nSleeping $SuspendDelay seconds to help make sure they have all finished booting.`nPress any key to continue..."
        try {
          $counter = 0
          while( !$Host.UI.RawUI.KeyAvailable -and ($counter++ -lt $SuspendDelay ))
          {
            Start-Sleep -Seconds 1
          }
        } catch {
          Write-Host "An error occurred (probably tried to read from a non-console).`n Sleeping $SuspendDelay ."
          Start-Sleep -Seconds $SuspendDelay
        }
        # They confirmed they wanted to revert - suspending is not destructive compared to that.
        Suspend-VM -VM $not_suspended -Confirm:$false
      }
    }
}
# https://dille.name/blog/2017/08/27/how-to-use-shouldprocess-in-powershell-functions/
#if (-not $PSBoundParameters.ContainsKey('Confirm')) {
#  $ConfirmPreference = $PSCmdlet.SessionState.PSVariable.GetValue('ConfirmPreference')
#}
#if (-not $PSBoundParameters.ContainsKey('WhatIf')) {
#  $WhatIfPreference = $PSCmdlet.SessionState.PSVariable.GetValue('WhatIfPreference')
#}

$ScriptName ="Manage-Snapshots"
$Version    ="0.1"
$Caption="$ScriptName : $Action"
$ScriptDir = "/home/terraform/terraform/powercli"
$SelectSnapshotsScript="$ScriptDir/select-snapshots.py"
$ConnectScript="$ScriptDir/connect.ps1"

if ($Connect) {
  & $ConnectScript
}

if (! $IDs ) {
  $IDs = @($Input)
}


$IDs = @($Input)

# Append Type info [if needed] so that Get-VM will work (Terraform does not store the type as part of the moid)
$IDs = $IDs | ForEach {
  $prefix="VirtualMachine-"
  if ($_.StartsWith($prefix)) {
    $_
  } else {
    $prefix + $_
  }
}



Write-Host ( "Applying Action {0} to {1} objects" -f $Action, $IDs.Count )
#Write-Host "ID: $IDs Action: $Action"

$VM=Get-VM -ID $IDs

switch ($Action) {

#################################################################################
  "Get" {
    # Just return the raw Snapshot objects so you can process them in the pipeline
    Write-Host ( "Getting snapshots from {0} VMs" -f $VM.Count )
    Get-Snapshot -VM $VM
    break;
  }

#################################################################################
  "GetReport" {

    # Example of creating a report to help manage a particular project
    #   Returns a report about the Snapshots being used by the project
    Write-Host ( "Getting snapshot sizes from {0} VMs" -f $VM.Count )
    #If you only want total size: Get-Snapshot -VM $VM
    $total_gb = 0.0
    $rows = New-Object System.Collections.Generic.List[System.Object]
    ForEach ($v in $VM) {
      $snapshots = Get-Snapshot -VM $v
      $num_snapshots = $snapshots.Length
      $size_gb = 0.0
      $snapshots | %{ $size_gb += $_.SizeGB }
      $total_gb += $size_gb 
      $first = "N/A"
      $last = "N/A"
      If ($num_snapshots -gt 0) {
        $first = $snapshots[0].Name
        $last = $snapshots[-1].Name
      }
      $col_names = $(
        "VM",
        "Num Snapshots",
        "Total Size of Snapshots (GB)",
        "First Snapshot",
        "Last Snapshot"
      )
      $row = @{
        $col_names[0]= "$v";
        $col_names[1] = $num_snapshots;
        $col_names[2] = [math]::Round($size_gb, 2)
        $col_names[3] = $first;
        $col_names[4] = $last;
      }
      $rows.Add($row)

    }
    MakeTable -Rows $rows -Columns $col_names
    $total_gb = [math]::Round($total_gb, 2)
    Write-Host "Grand Total Snapshot Size (GB): $total_gb"
    break;
  }

#################################################################################
  "RemoveAll" {
    # Remove ALL snapshots associated with this project, with prompts for each VM
    
    # Get-Snapshot + Remove-Snapshot

    # This would work, but wouldn't prompt for confirmation per VM
    # Get-Snapshot -VM $VM | Remove-Snapshot
    ForEach ($v in $VM) {
      $snapshots = Get-Snapshot -VM $v
      $num_snapshots = $snapshots.Length
      $size_gb = 0.0
      $snapshots | %{ $size_gb += $_.SizeGB }
      $size_gb = [math]::Round($size_gb, 2)
      if ($snapshots.Length -gt 0) {
        if ($PSCmdlet.ShouldProcess($Caption, "VM $v has $num_snapshots snapshots ($size_gb GB total). Delete them all?", $Caption) ) {
          $snapshots | Remove-Snapshot -Confirm:$false
        }
      } else {
        Write-Verbose "$v : No snapshots found!"
      }

    }
    break;
  }

#################################################################################
  "RemoveLast" {
    # Remove the most recent snapshot for each VM managed by this project

    Write-Host ( "Removing Last Snapshot from {0} VMs" -f $VM.Count )
    RemoveOne -VM $VM -index -1 -Caption $Caption -Desc "most recent"

    break;
  }

#################################################################################
  "RemoveFirst" {
    # Remove the most oldest snapshot for each VM managed by this project

    Write-Host ( "Removing First Snapshot from {0} VMs" -f $VM.Count )
    RemoveOne -VM $VM -index 0 -Caption $Caption -Desc "oldest"

    break;
  }

#################################################################################
  "Prune" {
    # Ensure no VM has more than -Retain snapshots
    # If some need to be delted, the indexes to delete are chosen by select-snapshots.py

    # It uses some rules to make sure the snapshots kept are reasoable
    # e.g. basic priority is last > first > last half > first half - snapshots are chosen to maximize coverage
    # Example: with -Retain 3 , the first and last snapshots will be kept, plus the middle-most snapshot
    
    # Get-Snapshot + select-snapshots.py + Remove-Snapshot

    Prune -VM $VM -Caption $Caption -Retain $Retain 
    break;
  }

#################################################################################
  "Backup" {
    # Ensure no VM has more than -Retain snapshots
    # If some need to be delted, the indexes to delete are chosen by select-snapshots.py

    # It uses some rules to make sure the snapshots kept are reasoable
    # e.g. basic priority is last > first > last half > first half - snapshots are chosen to maximize coverage
    # Example: with -Retain 3 , the first and last snapshots will be kept, plus the middle-most snapshot
    
    # Get-Snapshot + select-snapshots.py + Remove-Snapshot

    if (! $Name) {
      $Name = "VM Backup $(Get-Date -format u)"
    }
    if (! $Description) {
      $Description = "Created by $ScriptName v$Version [Backup] on `n$(Get-Date -format f)"
    }

    $VM | %{ New-Snapshot -VM $_ -Name $Name -Description $Description -Memory:$Memory -Quiesce:$Quiesce }

    Prune -VM $VM -Caption $Caption -Retain $Retain -NoInteractive 
    break;
  }

#################################################################################
  "New" {
    # Take a snapshot of the entire project. Supports all the options of New-Snapshot
    #  (Names and descriptions will contain good timestamps by default.
    #   If manually specifying values, use $(Get-Date -format u) for nice timestamps.)
    # New-Snapshot

    # Give all snapshots identical timestamps so they can be more easily correlated
    if (! $Name) {
      $Name = "VM Snapshot $(Get-Date -format u)"
      if ($Memory) {
        $Name = "[Memory] $Name"
      }
    }
    if (! $Description) {
      $Description = "Created by $ScriptName v$Version on `n$(Get-Date -format f)"
    }

    $VM | %{ New-Snapshot -VM $_ -Name $Name -Description $Description -Memory:$Memory -Quiesce:$Quiesce }
    break;
  }

#################################################################################
  "RevertToLast" {
    # Get-Snapshot + Set-VM
    Revert -VM $VM -index -1 -Caption $Caption "most recent" -DesiredPowerState $DesiredPowerState -SuspendDelay $SuspendDelay 
    break;
  }

#################################################################################
  "RevertToFirst" {
    # Get-Snapshot + Set-VM
    Revert -VM $VM -index 0 -Caption $Caption "oldest" -DesiredPowerState $DesiredPowerState -SuspendDelay $SuspendDelay 
    break;
  }
  default {
    Write-Error "ERROR: Action $Action is not supported!"
    return 1
  }
}

}
