function Terra-Connect {
  param (
    [Parameter(ParameterSetName='auth', Mandatory=$false)] [string] $CredsFile,
    [Parameter(ParameterSetName='auth', Mandatory=$false)] [string] $EnvCredsFile = 'TERRA_CREDS',
    [Parameter(Mandatory=$false)] [string] $Server,
    [Parameter(Mandatory=$false)] [string] $EnvServer = 'TERRA_VSPHERE',
    [Parameter(Mandatory=$false)] [switch] $Save,
    [Parameter(Mandatory=$false)] [switch] $ShowCreds
  )

    Set-PSReadLineOption -HistorySaveStyle SaveNothing
   
    if (-not $CredsFile -and (Test-Path Env:$EnvCredsFile) ){
        $CredsFile = Get-Content Env:$EnvCredsFile
    }
    if ($Save -and -not $CredsFile){
        Write-Error "-Save requires -File or a valid -EnvFile ($EnvCredsFile)" -Category InvalidArgument
        return $false
    }
    if (-not $Save -and -not $Server -and -not (Test-Path Env:$EnvServer)){
        Write-Error "Required: -Server or a valid -EnvServer ($EnvServer)" -Category InvalidArgument
        return $false
    }
    if (-not $Server){
        $Server = Get-Content Env:$EnvServer
    }

    $creds = $null
    if (-not $Save -and $CredsFile){
        $creds = (Import-Clixml -Path "$CredsFile").GetNetworkCredential()
        $username = $creds.UserName
        $password = $creds.Password
    } elseif ($Save){
        Write-Warning "These credentials will be saved to $CredsFile"
        Write-Warning "Ensure this file has appropriate permissions."
        $creds = Get-Credential
        $creds | Export-Clixml $CredsFile
        $creds = (Import-Clixml -Path "$CredsFile").GetNetworkCredential()
    }

    if (-not $Server){
        Write-Warning "-Server not given, exiting"
        return $false
    }

    if ($ShowCreds -and $creds) {
        Write-Host "{"
        Write-Host ('"vsphere_server":"{0}",' -f $Server)
        Write-Host ('"vsphere_user":"{0}",' -f $creds.UserName)
        Write-Host ('"vsphere_password":"{0}"' -f $creds.Password)
        Write-Host "}"
        return
    }
    if ($creds) {
        return Connect-ViServer -Server "$Server" -User $creds.UserName -Password $creds.Password -WarningAction silentlyContinue >$null
    } else {
        return Connect-ViServer -Server "$Server" -WarningAction silentlyContinue >$null
    }


}
