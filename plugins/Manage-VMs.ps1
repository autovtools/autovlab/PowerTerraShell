#!/snap/bin/pwsh -NoLogo

# Best - http://www.powertheshell.com/input_psv3/
# https://learn-powershell.net/2013/05/07/tips-on-implementing-pipeline-support/

# Because of how user interaction / consoles work, either run in a pwsh shell
# (where you will be prompted for confirmation on deletes by default)
# or use -Confirm:$false from bash if you are running non-interactively

function Manage-VMs {

# Note: -Action Set passes through most options to Set-VM
# However, some options are excluded intentionally
#   because they are deprecated, or don't make sense for or are not safe for project-level operations
[CmdletBinding(SupportsShouldProcess, ConfirmImpact='High')]
param(
  [parameter(Mandatory=$false)] [String[] ] $IDs,
  [parameter(Mandatory=$true)] [String] $Action,
  [parameter(Mandatory=$false)] [Switch] $Connect=$false,
  [parameter(Mandatory=$false)] [Int] $AttemptDelay=60,
  [parameter(Mandatory=$false)] [Int] $NumAttempts=5,
  [parameter(Mandatory=$false)] [Int] $SuspendDelay=240,
  [parameter(Mandatory=$false)] [Switch] $BootFirst=$false,
  [parameter(Mandatory=$false)] [Switch] $Full=$false,
  [parameter(Mandatory=$false)] [Switch] $NoInteractive=$false,
  [parameter(Mandatory=$false)] [Switch] $Force=$false,
  [parameter(Mandatory=$false)] [Switch] $Unresponsive=$false,

  [parameter(Mandatory=$false)] [String] $AlternateGuestName,
  [parameter(Mandatory=$false)] [String] $DrsAutomationLevel,
  [parameter(Mandatory=$false)] [String] $GuestId,
  [parameter(Mandatory=$false)] [String] $HAIsolationResponse,
  [parameter(Mandatory=$false)] [String] $HARestartPriority,
  [parameter(Mandatory=$false)] [Decimal] $MemoryGB,
  [parameter(Mandatory=$false)] [String] $Name,
  [parameter(Mandatory=$false)] [String] $Notes,
  [parameter(Mandatory=$false)] [Int] $NumCpu,
  [parameter(Mandatory=$false)] [Object] $OSCustomizationSpec,
  [parameter(Mandatory=$false)] [Object[]] $Server,
  [parameter(Mandatory=$false)] [Switch] $ToTemplate=$false,
  [parameter(Mandatory=$false)] [String] $Version,
  [parameter(Mandatory=$false)] [String] $VMSwapFilePolicy,
  

  [parameter(ValueFromPipeline=$true)]
    [String[]] $InputObject
)

# https://stackoverflow.com/questions/20874464/format-table-on-array-of-hash-tables
function MakeTable {
  param (
    [Object[] ] $Rows,
    [String []] $Columns
  )
  return $Rows | %{ [PSCustomObject]$_ } | Format-Table -Property $Columns -AutoSize 
}

# For some reason, it doesn't appear simple to get the datastore folder size.
# http://www.virtirl.com/determining-linked-clone-space-usage/
# and
# http://myvirtualcloud.net/how-to-read-linked-clone-storage-provisioning-metrics-in-vcenter/
# Both claim that "Unshared" storage for Linked Clones is the "real" storage and only considers
#   files in the VM's Datastore folder.
# But this seems to be wrong. "Unshared" seems to be everything but the swap file for Linked Clones

# The actual consumed space on the datastore is MUCH lower
# This computes the folder size of a VM's datastore folder

function GetDS {
  param (
    [Object] $VM
  )
  $view = $VM | Get-View
  $ds = $view.Storage.PerDatastoreUsage | % { Get-Datastore -id $_.DataStore }

  # WARNING: No idea what will happen for multiple datastores
  if ($ds.length -gt 1){
    $ds = $ds[0]
    Write-Warning "Multiple Datastores detected. Only considering first"
  }

  return $ds
}

# It isn't efficient, but if you are making a report, you can wait a bit
#   Only ran if you ask for the -Full report
# https://communities.vmware.com/thread/541055
function GetDataStoreFolderSize {
  [CmdletBinding()]
  param (
    [Object[] ] $VM
  )
  $total_persistent = 0.0
  $total_swap = 0.0

  ForEach ($v in $VM) {

    Write-Host "$v : Computing datastore folder size (actual used space)..."

    $ds_folder = Split-Path ($v | Get-HardDisk).Filename.split("]")[1].TrimStart()
    # Mount the datastore as a network drive
    $drive = "DS"
    Write-Verbose "Drive: $drive"

    $files = Get-ChildItem -Path "$($drive):\$ds_folder"
    $swap_files = $files | ? { $_.Name -match ".*\.vswp" }
    $persistent_files = $files | ? { $_.Name -notmatch ".*\.vswp" }

    $swap_size = ( $swap_files | Measure-Object -Property Length -Sum | select -ExpandProperty Sum ) / 1GB
    $persistent_size = ( $persistent_files | Measure-Object -Property Length -Sum | select -ExpandProperty Sum ) / 1GB

    $total_persistent += $persistent_size
    $total_swap += $swap_size

    Write-Host ("Found $($files.Length) files {0}GB/{1}GB/{2}GB Persistent/Swap/Total" -f [math]::Round($persistent_size, 2) , [math]::Round($swap_size, 2) , [math]::Round($persistent_size + $swap_size, 2) )
  } 

  return @( $total_persistent, $total_swap, ($total_persistent + $total_swap) )
}

# Refresh the state of the VM objects by re-querying vSphere
# (Note: if you changed the state of the VM, e.g by powering it off, you must refresh to get the new PowerState)
function Refresh {
  param(
    [Object[] ]$VM
  )
  if (! $VM ) {
    Write-Verbose "Cannot refresh empty list!"
    return $VM
  }
  $IDs = $VM | %{ $_.Id }
  return Get-VM -Id $IDs 
}

function ForceIfNeeded {
[CmdletBinding(SupportsShouldProcess, ConfirmImpact='High')]
 param(
  [Object[] ]$VM,
  [parameter(Mandatory=$false)] [String] $NiceCmd,
  [parameter(Mandatory=$false)] [String] $ForceCmd,
  [parameter(Mandatory=$false)] [String] $Desc,
  [parameter(Mandatory=$false)] [String] $DesiredPowerState,
  [parameter(Mandatory=$false)] [Int] $AttemptDelay,
  [parameter(Mandatory=$false)] [Int] $NumAttempts,
  [parameter(Mandatory=$false)] [Switch] $Force=$false,
  [String] $Caption
 )

  # Could be done with: Stop-VM -VM $VM
  # But we want to prompt the user nicely and follow through with a PowerOff if something fails
  # And we want to leave things in a predictable state:
  #   e.g. "Rebooting" a project should leave every VM On; but a simple Restart command will fail if a VM is already off.

  $failed = New-Object System.Collections.Generic.List[System.Object]
  $cancelled = New-Object System.Collections.Generic.List[System.Object]

  # Ask nicely
  ForEach ( $v in $VM ) {
    $power_state = $v.PowerState 
    if ($v.PowerState -eq "PoweredOff") {
      Write-Verbose "$v : Currently PoweredOff"
      if ($DesiredPowerState -ne "PoweredOff") {
        $v | Start-VM 
      }
      # If this is a Start or a Restart, just turn it on and skip the rest - I don't think that can fail
      # (if it did, there is no way to "force" start a VM)
      if ($power_state -eq "PoweredOff") {
        Write-Verbose "$v : Booted from $power_state. No need to restart"
        continue
      } else {
        Write-Verbose "$v : Booted from $power_state."
        # Fall through - unsuspend didn't really give us a fresh boot
      }
    }
    $msg = "$v : Attempting to $Desc VM. Continue?"
    if ($PSCmdlet.ShouldProcess($Caption, $msg, $Caption) ) {
      & $NiceCmd -VM $v -Confirm:$false
      if (! $? ) {
        $warning = "$v : First attempt to $Desc ($NiceCmd) failed. "
        if ($Force) {
          $warning += "It will be forced shortly..."   
        }
        else {
          $warning += "$NumAttempts attempts remain ($AttemptDelay second delay)"   
        }
        Write-Warning $warning
        $failed.Add($v)
      }
    }
    else {
      $cancelled.Add($v.Id)
    }
  } 

  # Remove from consideration anything the user said no to
  $VM = $VM | ? { $_.id -notin $cancelled } 

  if ($DesiredPowerState -eq "PoweredOff" -and $failed.Count -lt $VM.Length ) {
    # Give things a chance to turn off
    $delay = $AttemptDelay / 2
    Write-Host "Waiting $delay seconds for VMs to $Desc..."
    Start-Sleep -Seconds $delay
  }

  # Ask nicely again for anything that failed [according to the patience of the user]
  if ($Force) {
    Write-Host "Forcing ..."

    $VM = Refresh -VM $VM
    if ($DesiredPowerState -eq "Restart") {
      Write-Warning "$($failed.Count) VMs threws errors. Forcing: $DesriedPowerState ."

      $failed | %{ & $ForceCmd -VM $_ -Confirm:$false }
    } else {
      $failed = $VM | ? { $_.PowerState -ne $DesiredPowerState }
      
      Write-Warning "$($failed.Count) VMs are not $DesiredPowerState yet. They will be forced [$DesiredPowerState] now."
     
      ForEach ($f in $failed) {
        & $NiceCmd -VM $f -Confirm:$false
        if (! $? ) {
          & $ForceCmd -VM $f -Confirm:$false
        }
      } 
    }

  } else {
  
    $NumAttempts--
    for ($i=$NumAttempts; $i -gt 0; $i--) {
      # Retry things we know failed first, to give slow VMs a chance to fulfill their promises
      $failed_again = New-Object System.Collections.Generic.List[System.Object]
      ForEach ($f in $failed) {
        if ($f.PowerState -ne $DesiredPowerState ) {
          Write-Host "$v : Retrying..." 
          & $NiceCmd $f -Confirm:$false
          if ( ! $?) {
            $failed_again.Add($f)
          }
        } else {
          Write-Host "$v is now $DesiredPowerState . Skipping." 
          continue
        }

      }
      if ($failed_again) {
        Write-Host "$( $failed_again.Count ) VMs reported failures ($NiceCmd - $failed_again) "
      }

      $VM = Refresh -VM $VM
      if ($DesiredPowerState -eq "Restart") {
        $off = $VM | ? { $_.PowerState -ne "PoweredOn" }
        $off_ids = $off | % { $_.Id }

        if ($off) { $off | Start-VM }

        # Things that were powered off / we powered on aren't really failed
        $failed = $failed_again | ? { ! ( $_.Id -in $off_ids ) }  


      } else {
        $failed = $VM | ? { $_.PowerState -ne $DesiredPowerState }
      }
  
      if ($failed.Count -gt 0) {
        Write-Warning "$($failed.Count) VMs are not $DesiredPowerState yet. $i attempts remain ($AttemptDelay second delay)"
   
        Start-Sleep -Seconds $AttemptDelay
        $failed = Refresh -VM $failed
      } else { break; }
       
    }

    # Final check / cleanup
    if ($DesiredPowerState -ne "Restart") {
      $VM = Refresh -VM $VM
      $failed = $VM | ? { $_.PowerState -ne $DesiredPowerState }
    }
    # Force anything that failed
    if ($failed.Count -gt 0) {
      Write-Warning "Final: $($failed.Count) VMs. Forcing $DesiredPowerState now ."
      $failed | %{ & $ForceCmd -VM $_ -Confirm:$false }
    }
    


  }
  

}

# https://dille.name/blog/2017/08/27/how-to-use-shouldprocess-in-powershell-functions/
#if (-not $PSBoundParameters.ContainsKey('Confirm')) {
#  $ConfirmPreference = $PSCmdlet.SessionState.PSVariable.GetValue('ConfirmPreference')
#}
#if (-not $PSBoundParameters.ContainsKey('WhatIf')) {
#  $WhatIfPreference = $PSCmdlet.SessionState.PSVariable.GetValue('WhatIfPreference')
#}

$ScriptName ="Manage-VMs"
$Version    ="0.1"
$Caption="$ScriptName : $Action"

$ScriptDir = "/home/terraform/terraform/powercli"
$ConnectScript="$ScriptDir/connect.ps1"

if ($Connect) {
  & $ConnectScript
}

if (! $IDs ) {
  $IDs = @($Input)
}

# Append Type info [if needed] so that Get-VM will work (Terraform does not store the type as part of the moid)
$IDs = $IDs | ForEach {
  $prefix="VirtualMachine-"
  if ($_.StartsWith($prefix)) {
    $_
  } else {
    $prefix + $_
  }
}



Write-Host ( "Applying Action {0} to {1} objects" -f $Action, $IDs.Count )
#Write-Host "ID: $IDs Action: $Action"

$VM=Get-VM -ID $IDs

switch ($Action) {

#################################################################################
  "Get" {
    # Just return the raw Snapshot objects so you can process them in the pipeline
    Write-Host ( "Getting {0} VMs" -f $VM.Count )
    $VM
    break;
  }

#################################################################################
# WARNING: Something will almost certainly will fail if the VMs are not on the same datastore
  "GetReport" {

    # Example of creating a report to help manage a particular project
    #   Returns a report about project VMs and their consumed resources
    Write-Host ( "Making Project Report for {0} VMs" -f $VM.Count )

    $total_gb_folder = 0.0
    $total_gb_unshared = 0.0
    $total_gb_used = 0.0
    $total_gb_prov = 0.0
    $total_ram = 0.0
    $total_cpu = 0

    $col_names = $(
      "VM",
      "Folder",
      "Real (PoweredOn) (GB)",
      "Real (PoweredOff) (GB)",
      "Unshared (GB)",
      "Used (GB)",
      "Provisioned (GB)",
      "RAM (GB)",
      "# CPUs"
    )
    $rows = New-Object System.Collections.Generic.List[System.Object]

    $mounted = $false 

    ForEach ($v in $VM) {

      if ( ( ! $mounted ) -and $Full ) {
        Write-Host "Creating tmp drive"
        New-PSDrive -Location $(GetDS -VM $v ) -Name "DS" -PSProvider VimDatastore -Root "\" > $null
        $mounted = $true
        Write-Host "Mounted: $mounted"
      }

      $folder_size_persistent = 0
      $folder_size_swap = 0
      $folder_size_total = 0

      # Tally up resource counts
      if ($Full) {
        $arr = GetDataStoreFolderSize -VM $v
        $folder_size_persistent = $arr[0]
        $folder_size_swap = $arr[1]
        $folder_size_total = $arr[2]
      }

      # http://www.virtirl.com/determining-linked-clone-space-usage/
      # Note: even though the link claims it does not, this appears to count the parent disk for linked clones
      $unshared = ($v | Get-View).Storage.PerDataStoreUsage.Unshared / 1GB
      
      $total_gb_folder += $folder_size_total
      $total_gb_persistent += $folder_size_persistent
      $total_gb_unshared += $unshared
      $total_gb_used += $v.UsedSpaceGB
      $total_gb_prov += $v.ProvisionedSpaceGB
      $total_ram     += $v.MemoryMB / 1024.0
      $total_cpu     += $v.NumCpu

      $row = @{
        $col_names[0]= "$v";
        $col_names[1] = $v.Folder;
        $col_names[2] = [math]::Round( $folder_size_total, 2) ;
        $col_names[3] = [math]::Round( $folder_size_persistent, 2) ;
        $col_names[4] = [math]::Round( $unshared , 2);
        $col_names[5] = [math]::Round($v.UsedSpaceGB, 2);
        $col_names[6] = [math]::Round($v.ProvisionedSpaceGB, 2);
        $col_names[7] = [math]::Round($v.MemoryMB / 1024.0, 2);
        $col_names[8] = $v.NumCpu;
      }
      $rows.Add($row)

    }
    # Make the Total row
    $real_size = "[-Full]"
    $real_size_persistent = $real_size
    if ($Full) {
      $real_size = [math]::Round($total_gb_folder, 2)
      $real_size_persistent = [math]::Round($total_gb_persistent, 2)
    }
  
    $row = @{
      $col_names[0]= "PROJECT TOTAL";
      $col_names[1] = "";
      $col_names[2] = $real_size ;
      $col_names[3] = $real_size_persistent ;
      $col_names[4] = [math]::Round($total_gb_unshared, 2);
      $col_names[5] = [math]::Round($total_gb_used, 2);
      $col_names[6] = [math]::Round($total_gb_prov, 2);
      $col_names[7] = [math]::Round($total_ram, 2);
      $col_names[8] = $total_cpu;
    }
    $rows.Add($row)

    if ($mounted ) {
      Write-Host "Removing tmp drive"
      Remove-PSDrive -Name "DS"
    }

    # Make the table
    MakeTable -Rows $rows -Columns $col_names


    break;
  }
#################################################################################
  "Start" {
    Write-Host "Starting $($VM.Length) VMs..."
    $VM | ? {$_.PowerState -ne "PoweredOn"} | Start-VM
    break;
  }

#################################################################################
  "Suspend" {
    Write-Host "Suspending $($VM.Length) VMs..."
    $suspend_queue = New-Object System.Collections.Generic.List[System.Object]

    ForEach ($v in $VM) {
      if ($v.PowerState -ne "Suspended") {
        if ($v.PowerState -eq "PoweredOff" -and $BootFirst ) {
          $msg = "$v : Currently $($v.PowerState). Boot first and suspend in ~ $SuspendDelay seconds?"
          if ($PSCmdlet.ShouldProcess($Caption, $msg, $Caption) ) {
            $v | Start-VM 
            $suspend_queue.Add($v)
          }
        }
        elseif ($v.PowerState -eq "PoweredOff") {
          Write-Warning "$v : VM is $($v.PowerState) , so it will not be suspended. Run with -BootFirst to ensure all VMs are suspended."
          continue
        }
        else {
          $msg = "$v : VM will be suspended. Continue?"
          if ($PSCmdlet.ShouldProcess($Caption, $msg, $Caption) ) {
            $v | Suspend-VM -Confirm:$false
          }
        }
      } else {
        Write-Verbose "$v is already suspended..."
      }
    }

    if ($suspend_queue.Count -gt 0) {
      Write-Host "Waiting $SuspendDelay seconds for $($suspend_queue.Count) VMs to boot..."
      Start-Sleep -Seconds $SuspendDelay
      Write-Host "Suspending $($suspend_queue.Count) VMs..."
      $suspend_queue | Suspend-VM -Confirm:$false 
    }
    break;
  }

#################################################################################
  "Stop" {
    ForceIfNeeded -VM $VM -NiceCmd "Stop-VMGuest" -ForceCmd "Stop-VM" -Desc "power off" -DesiredPowerState "PoweredOff" -AttemptDelay $AttemptDelay -NumAttempts $NumAttempts -Force:$Force -Caption $Caption
    break;
  }

#################################################################################
  "Restart" {
    # A common use case is to force restart anything that hung.
    # These VMs will not have VMWare Tools running
    if ($Unresponsive) {
      # User only wants to restart unresponsive / hung VMs
      # (toolsOk is the other status)
      $VM = $VM | ? { $_.ExtensionData.Guest.ToolsStatus -eq "toolsNotRunning" }
      $off = $VM | % { if($_.PowerState -eq "PoweredOff") {$_.id}  }

      # Start anything that is off; reset the rest
      if ($off) {
        Start-VM -Id $off
      } 
      $VM = $VM | ? {$_.Id -notin $off }

      if ($VM) {
        $VM | Restart-VM
      }

    }
    else {
      ForceIfNeeded -VM $VM -NiceCmd "Restart-VMGuest" -ForceCmd "Restart-VM" -Desc "restart" -DesiredPowerState "Restart" -AttemptDelay $AttemptDelay -NumAttempts $NumAttempts -Force:$Force -Caption $Caption
    }
    break;
  }

#################################################################################
# Passthru to Set-VM
# https://pubs.vmware.com/vsphere-51/index.jsp?topic=%2Fcom.vmware.powercli.cmdletref.doc%2FSet-VM.html
  "Set" {

    # Reproduce the argument list for the 3rd time :(
    $supported_args = @(
      "AlternateGuestName"
      "DrsAutomationLevel"
      "GuestId"
      "HAIsolationResponse"
      "HARestartPriority"
      "MemoryGB"
      "Name"
      "Notes"               
      "NumCpu"              
      "OSCustomizationSpec" 
      "Server"
      "ToTemplate"
      "VMSwapFilePolicy"
    )

    $set_args = @{}
    $PSBoundParameters.Keys | % { if ($_ -in $supported_args ) { $set_args[$_] = $PSBoundParameters[$_] } }

    # Let users cancel individually
    ForEach ($v in $VM){
      $msg = "$v : Call Set-VM with these args?`n{0}" -f ($set_args | Format-Table -AutoSize | Out-String)
      if ($PSCmdlet.ShouldProcess($Caption, $msg, $Caption) ) {
        $v | Set-VM @set_args -Confirm:$false
      }
    }


  }
#################################################################################
  default {
    Write-Error "ERROR: Action $Action is not supported!"
    return 1
  }
}


}
