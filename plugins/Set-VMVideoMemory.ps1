# https://www.virtu-al.net/2011/03/10/change-vm-video-memory/
function Set-VMVideoMemory {
param (
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $VM,
  [int64]$MemoryMB = 32,
  [bool]$AutoDetect = $false,
  [ValidateSet(1,2,3,4)]
    [int]$NumDisplays = 1
)

  if ($Name){
        $VM = Get-VM $Name
    } else {
        $VM = @($input)
    }

    if ( -not $VM) {
        Write-Error "No VMs given" -Category InvalidArgument
        return
    }
 
   $VM | Foreach {
      $VideoAdapter = $_.ExtensionData.Config.Hardware.Device | Where {$_.GetType().Name -eq "VirtualMachineVideoCard"}
      $spec = New-Object VMware.Vim.VirtualMachineConfigSpec
      $Config = New-Object VMware.Vim.VirtualDeviceConfigSpec
      $Config.device = $VideoAdapter
      If ($MemoryMB) {
         $Config.device.videoRamSizeInKB = $MemoryMB * 1KB
      }
      If ($AutoDetect) {
         $Config.device.useAutoDetect = $true
      } Else {
         $Config.device.useAutoDetect = $false
      }
      $Config.device.numDisplays = $NumDisplays

      $Config.operation = "edit"
      $spec.deviceChange += $Config
      $VMView = $_ | Get-View
      Write-Verbose "Setting Video Display for $($_)"
      $VMView.ReconfigVM($spec)
  }

}
