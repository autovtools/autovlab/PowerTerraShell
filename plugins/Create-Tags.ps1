#!/snap/bin/pwsh -NoLogo
# One-time creation of any tag categories + tags used by other tools

function Create-Tags{

[CmdletBinding()]
param()

    New-TagCategory -Name "os_family" -Description "Generic OS category, e.g. Linux, Windows, BSD, Other" -Cardinality Single
    New-TagCategory -Name "os_flavor" -Description "Linux distro / Windows version" -Cardinality Single
    New-TagCategory -Name "os_variant" -Description "Variant of the os (e.g Server, Desktop, GNOME, KDE)" -Cardinality Single
    New-TagCategory -Name "os_version" -Description "Linux release / Windows build number" -Cardinality Single
    New-TagCategory -Name "os_arch" -Description "CPU architecture of the image (e.g x86 / x86_64)" -Cardinality Single
    New-TagCategory -Name "build_timestamp" -Description "Date / time of image build" -Cardinality Single

    foreach ($family in @("Linux", "BSD", "Windows", "MacOS", "Other")) {
        New-Tag -Name $family -Category "os_family"
    }

}
