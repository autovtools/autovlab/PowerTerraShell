#!/snap/bin/pwsh -NoLogo

# Turn a new VM into a ready-to-copy VM
# VMs must be PoweredOff
# WARNING: Deletes all previous Snapshots without confirmation
#  (Terraform won't make a linked clone if there is more than one)
function Prep-Template{

[CmdletBinding()]
param(
  [parameter(Mandatory=$false)] [string[] ] $Name,
  [parameter(ValueFromPipeline=$true)] [Object[] ] $VM,
  [switch] $Snapshot = $true,
  [string] $SnapshotName,
  [string] $SnapshotDesc
)
    if ($Name){
        $VM = Get-VM $Name
    } else {
        $VM = @($input)
    }

    if ( -not $VM) {
        Write-Error "No VMs given" -Category InvalidArgument
        return
    }

    $VM | Enable-CopyPaste

    $VM | Set-VMVideoMemory -MemoryMB 32 -NumDisplays 1

    if ($Snapshot) {
      if (! $SnapshotName ){
        $SnapshotName = "Template_Snapshot $(Get-Date -format u)"
      }
      if (! $SnapshotDesc) {
        $SnapshotDesc = "Created by Prep-Template`n$(Get-Date -format f)"
      }
      $old = $VM | Get-Snapshot
      if ($old){
        # Prompts user if they are sure, which is good safety bc
        #   automated use cases generally should not have existing Snapshots
        Write-Warning ("Deleting {0} old snapshot(s)..." -f $old.Length)
        $old | Remove-Snapshot -Confirm:$false
      }
      $VM | New-Snapshot -Name $SnapshotName -Description $SnapshotDesc -Memory:$false | Out-Null
    }

}
