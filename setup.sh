#!/bin/bash

# Usage:
# cd /path/to/terrashell
# ./setup.sh

# Works on Ubuntu
sudo apt install snap
sudo snap install powershell --classic

./setup.ps1
sudo ln $(pwd)/terrashell -s /usr/local/bin/terrashell
# You may need to re-exec bash for it to be found in your path

cat <<EOF
To use terrashell, you must set some environment variables:

echo 'export TERRA_VSPHERE=vcenter.example.localdomain' >> ~/.bashrc
echo 'export TERRA_CREDS=/somewhere/safe/powercli.creds' >> ~/.bashrc


And create a credential file (in that location) for terrashell to use:

# Terrashell will complain until we set everything up correctly
terrashell
# In Terrashell, create the credential file
Terra-Connect -CredsFile /somewhere/safe/powercli.creds -Save

# Once you're all done, re-exec bash (or logout / login) and try running:
terrashell
# There should be no warnings; exit when done
EOF
