# terrashell

A collection of `PowerCLI` functions intended to simplify and automate `vSphere` infrastructure management.

# Role

`TerraShell` is designed to behave as an interactive vSphere infrastructure management shell that works from Linux.

General `IAC` lifecycle:

1. Building VM Templates: `packinator` (`packer`)
2. Deploying VM Templates: `PyTerraDactSL` (`terraform`)
3. Configuring VM Templates: `SaltStack`
4. Managing Deployed Infrastructure
    * Guest Management: `SaltStack`
    * \*VM Management: `terrashell`

# Setup

The intended platform is `Linux` (`ubuntu` works well).

```bash
cd /path/to/terrashell
# Modifiy setup.sh as needed;
# As long as your distro supports powershell
#   (preferably via snap), you can probably make it work
./setup.sh
```

